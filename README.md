# Ambisafe KeyServer Java client

## Prerequisites

* Java SE Runtime Environment 8

## Server Side Usage Examples

To use client you need to add jar with dependencies `client-java-<VERSION>-jar-with-dependencies.jar` to your project. 
Client gives possibility to make authenticated requests to Keyserver. 

	
	>	` ResponseEntity<String> response = AuthenticatedRequest.makeAndExecute("http://localhost:8080/balances/BTC/address/1FQSHsxjq3Ui8Grtg1dWkhCYAK3AWg3mmR", HttpMethod.GET, "", "demo", "demo");` 

Check `Example` class how to create container and account of `UserSideKey` security schema.

## Android Usage Examples
### Example 1: get current balance for an specific address
This example works with the keyserver java application deployed on localhost, port 8080.
	
### Example 2: create account & save
This supposed to happen after user have filled registration form and clicked submit:

* Create an account with the `AccountService.generateAccount` function. 
	* The **currency** value is required.
	* The **password** value is required.
	* The **salt** value is not required.

	> 	**Code**:  	
	> ``` account = AccountService.generateAccount(currency, password, salt); 	```
	> 
	> 	**Example**:  	
	> ``` 	CryptoService cryptoService = new CryptoService();
	>     String salt = cryptoService.getSalt();
	>     Account account = AccountService.generateAccount("BTC", "password", salt); 	```

* To convert the **account** attributes to a JSON string, execute the following line: `accountSerialized = account.toString();`


### Example 3: sign in & create transaction
Container with account JSON should be fetched from a server after successful login.The most optimal way to fetch it depends client application and therefore delegated to it.

* Decrypting account with private key: account json as it returned by **Server + password**, entered during authentication. Password should **never** be transmitted to server. Exception can be thrown here if password is incorrect.

	> 	**Code** 	
	> ``` 	account = new co.ambisafe.client.Account(containerJson, password); 	```
	> 
	> 	**Example** 	
	> ``` 	account = new co.ambisafe.client.Account(containerJson, password); 	```

		
* Constructing transaction & validating against business logic rules like with account, creation and validation of transaction is out of scope of this library.
* Signing transaction with customer key:

	> 	**Code** 	 	
	> ``` 	signedTransaction = AccountService.signTransaction(transaction, privateKey); 	```
	> 
	> 	**Example** 	 	
	> ``` 	String txString = "{'hex': '2340','fee': '0.0001 BTC','sighashes': ['73020cb8c25f434e00473dcd71be5bcfc0adaf107e0b0f36499d5abf8bd2da18']}";
	>     JSONParser parser = new JSONParser();
	>     JSONObject tx = (JSONObject) parser.parse(txString.replaceAll("'", "\""));
	>     JSONObject signedTx = account.signTransaction(tx); 	```

* Signed transaction should be submitted to server for co-signing and broadcasting.

### Example 4: change password
To change the password of a created account the user has to call to the `setNewPassword` function:

* Based on a created account: 

	> 	**Code** 	 	
	> ``` 	Account account = AccountService.generateAccount(currency, password, salt); 	```
	>
	> 	**Example** 	
	> ``` 	Account account = AccountService.generateAccount("BTC", "password", (new CryptoService()).getSalt()); 	```

* Call the **setNewPassword** function with the **newEncryptionKey** attribute: 

	> 	**Code** 	 	
	> ``` 	account.setNewPassword(newEncryptionKey); 	```
	>
	> 	**Example** 	 	
	> ``` 	account.setNewPassword("newpassword"); 	```

## Ambisafe License and Service Agreement
Ambisafe License and Service Agreement (ALSA)

This code is a property of Ambisafe Inc. No use without written permission from Ambisafe Inc. is allowed.