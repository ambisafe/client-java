package co.ambisafe.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.json.simple.JSONObject;

public class Container {
    private int role;
    @JsonProperty("public_key")
    private String publicKey;
    private String data;
    private String salt;
    private String iv;

    public Container() {
    }

    public Container(int role, String publicKey, String data, String salt, String iv) {
        this.role = role;
        this.publicKey = publicKey;
        this.data = data;
        this.salt = salt;
        this.iv = iv;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getIv() {
        return iv;
    }

    public void setIv(String iv) {
        this.iv = iv;
    }

    String toJsonString() {
        JSONObject object = new JSONObject();

        object.put("role", role);
        object.put("public_key", publicKey);
        object.put("data", data);
        object.put("salt", salt);
        object.put("iv", iv);

        return object.toJSONString();
    }
}
