package co.ambisafe.client;

import co.ambisafe.utils.ECKey;

public interface ContainerService {

    Container createContainer(int role, String secret);

    Container createContainer(ECKey ecKey, int role, String secret);
}
