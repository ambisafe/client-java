package co.ambisafe.client;

import co.ambisafe.utils.PBKDF2SHA512;
import org.spongycastle.crypto.InvalidCipherTextException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.ShortBufferException;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.UUID;

public class CryptoService {

    // Iteration count is set to 1000
    // Desired length of thex
    // derived key is 256 bits (= 32 bytes).
    private final int keyLength = 32;
    private final int iterations = 1000;

    public byte[] encryptData(byte[] data, byte[] iv, String salt, String secret) throws InvalidCipherTextException, BadPaddingException, ShortBufferException, IllegalBlockSizeException, IOException {
        AesCbcCrypto aesCbcCrypto = new AesCbcCrypto();
        prepare(aesCbcCrypto, iv, salt, secret);
        return aesCbcCrypto.encrypt(data);
    }

    public byte[] decryptData(byte[] data, byte[] iv, String salt, String secret) throws InvalidCipherTextException, BadPaddingException, ShortBufferException, IllegalBlockSizeException, IOException {
        AesCbcCrypto aesCbcCrypto = new AesCbcCrypto();
        prepare(aesCbcCrypto, iv, salt, secret);
        return aesCbcCrypto.decrypt(data);
    }

    public byte[] getRandomIV() {
        byte[] iv = new byte[16];
        new SecureRandom().nextBytes(iv);
        return iv;
    }

    public String getSalt() {
        return UUID.randomUUID().toString();
    }

    private void prepare(AesCbcCrypto aesCbcCrypto, byte[] iv, String salt, String secret) {
        aesCbcCrypto.resetCiphers();
        aesCbcCrypto.setKey(PBKDF2SHA512.derive(secret, salt, iterations, keyLength));
        aesCbcCrypto.setIV(iv);
        aesCbcCrypto.initCiphers();
    }

    public byte[] hash256(String data) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(data.getBytes());
        return md.digest();
    }
}