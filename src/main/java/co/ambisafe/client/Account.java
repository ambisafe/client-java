package co.ambisafe.client;

import java.io.IOException;
import java.util.Iterator;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.ShortBufferException;

import co.ambisafe.utils.Base16Encoder;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.spongycastle.crypto.InvalidCipherTextException;

public class Account {
	private JSONObject data;

	/**
	 * Defines the Account constructor.
	 *
	 * @return none.
	 */
	public Account() {
		data = new JSONObject();
	}

	/**
	 * Defines the Account constructor.
	 *
	 * @param {string}
	 *            containerJson.
	 * @param {string}
	 *            password.
	 * @return none.
	 * @throws IOException
	 * @throws IllegalBlockSizeException
	 * @throws ShortBufferException
	 * @throws BadPaddingException
	 * @throws InvalidCipherTextException
	 */
	public Account(String containerJson, String password) throws Exception {
		if (containerJson == null) {
			return;
		}

		JSONObject accountData;
		JSONObject userData;
		String key;
		byte[] privateKey;
		data = new JSONObject();

		CryptoService cryptoService = new CryptoService();
		
		if (!(containerJson instanceof String)) {
			throw new Exception("Expect parameter 1 to be string");
		}

		try {
			JSONParser parser = new JSONParser();
			accountData = (JSONObject) parser.parse(containerJson);
		} catch (Exception e) {
			throw new Exception("Invalid JSON");
		}

		if (accountData.get("containers") == null) {
			throw new Exception("The attribute containers is not defined");
		}

		JSONObject containers = (JSONObject) accountData.get("containers");

		if (containers.get("USER") == null) {
			throw new Exception("The attribute containers.USER is not defined");
		}

		userData = (JSONObject) containers.get("USER");
		userData.remove("role");
		
		Iterator<?> keys = userData.keySet().iterator();

		while (keys.hasNext()) {
			String userDataKey = (String) keys.next();
			this.set(userDataKey, userData.get(userDataKey));
		}

		if (data.get("salt") != null && data.get("data") != null && data.get("iv") != null && password != null) {
			key = AccountService.deriveKey(password, (String) data.get("salt"), 1000);
			this.set("key", key);
			
			privateKey = cryptoService.decryptData(Base16Encoder.decode((String) data.get("data")), Base16Encoder.decode((String)
					data.get("iv")), (String) data.get("salt"), key);

			this.set("privateKey", privateKey);
		}
	};

	/**
	 * Instance method that signs a transaction.
	 *
	 * @param {object}
	 *            unsigned transaction: {hex:"...", fee:"...", sighashes:["...",
	 *            "..."]}.
	 * @return {object} signed transaction.
	 * @throws Exception
	 */
	public JSONObject signTransaction(JSONObject tx) throws Exception {
		String privateKey = (String) this.data.get("privateKey");

		if (privateKey != null) {
			return AccountService.signTransaction(tx, privateKey);
		}

		throw new Exception("The transaction was not signed. privateKey is not defined");
	};

	/**
	 * Instance method that set a new password
	 *
	 * @param {string} password
	 * @return none.
	 * @throws IOException 
	 * @throws IllegalBlockSizeException 
	 * @throws ShortBufferException 
	 * @throws BadPaddingException 
	 * @throws InvalidCipherTextException 
	 */
	public void setNewPassword(String password) throws Exception {
		if (this.get("salt") == null || this.get("data") == null || this.get("iv") == null) {
			throw new Exception("The following attributes are required: salt, data and iv.");
		}

		CryptoService cryptoService = new CryptoService();

		String curKey = (String) this.get("key");
		byte[] curData = Base16Encoder.decode((String) this.get("data"));

		byte[] privateKey = cryptoService.decryptData(curData, Base16Encoder.decode((String) this.get("iv")), (String) this.get("salt"), curKey);

		String newKey = AccountService.deriveKey(password, (String) this.get("salt"), 1000);

		this.set("iv", Base16Encoder.encode(cryptoService.getRandomIV()));
		byte[] newData = cryptoService.encryptData(privateKey, Base16Encoder.decode((String) this.get("iv")), (String) this.get("salt"), newKey);

		this.set("data", Base16Encoder.encode(newData));
		this.set("key", newKey);
		this.set("privateKey", Base16Encoder.encode(privateKey));
	}

	/**
	 * Intance method that returns the Account"s data in a JSON format
	 *
	 * @param none.
	 * @return {string} return the account data as string.
	 */
	public String stringify() {
		return this.data.toJSONString();
	};

	/**
	 * Intance method that parse the Account"s data
	 *
	 * @param {string}
	 *            return the account data as string
	 * @return none.
	 * @throws ParseException
	 */
	public void parse(String data) throws ParseException {
		JSONParser parser = new JSONParser();
		this.data = (JSONObject) parser.parse(data);
	};

	/**
	 * Intance method that get the Account"s container as a Javascript object
	 *
	 * @param none.
	 * @return {object}
	 */
	@SuppressWarnings("unchecked")
	public JSONObject getContainer() {
		JSONObject container = new JSONObject();

		String publickey = (String) this.data.get("publicKey");
		String data = (String) this.data.get("data");
		String salt = (String) this.data.get("salt");
		String iv = (String) this.data.get("iv");

		container.put("publicKey", publickey);
		container.put("data", data);
		container.put("salt", salt);
		container.put("iv", iv);

		return container;
	};

	/**
	 * Intance method that get the Account"s container as string
	 *
	 * @param none.
	 * @return {string}
	 */
	public String getStringContainer() {
		return this.getContainer().toJSONString();
	}

	/**
	 * Instance method that gets the value of an indicated attribute.
	 *
	 * @param {string}
	 *            attribute name.
	 * @return {object} return the value of the indicated attribute.
	 */
	public Object get(String key) {
		return this.data.get(key);
	}

	/**
	 * Instance method that sets the value of an indicated attribute.
	 *
	 * @param {string}
	 *            attribute name.
	 * @param {object}
	 *            attribute value.
	 * @return none.
	 */
	@SuppressWarnings("unchecked")
	public void set(String key, Object value) {
		this.data.put(key, value);
	}

}