package co.ambisafe.client;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.GeneralSecurityException;
import java.util.Base64;

public class HMACUtils {
    public static String calculateHMAC(String secret, String data) {
        try {
            SecretKeySpec signingKey = new SecretKeySpec(secret.getBytes(), "HmacSHA512");
            Mac mac = Mac.getInstance("HmacSHA512");
            mac.init(signingKey);
            byte[] rawHmac = mac.doFinal(data.getBytes());
            return new String(Base64.getEncoder().encode(rawHmac));
        } catch (GeneralSecurityException e) {
            throw new IllegalArgumentException();
        }
    }

    public static String calculateContentToSign(String url, String method, String content, String timestamp) {
        // calculate content to sign
        return new StringBuilder(timestamp).append("\n")
                .append(method).append("\n")
                .append(url).append("\n")
                .append(content).toString();
    }
}
