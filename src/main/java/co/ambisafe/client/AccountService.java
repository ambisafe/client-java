package co.ambisafe.client;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Formatter;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.ShortBufferException;

import co.ambisafe.utils.Base16Encoder;
import co.ambisafe.utils.ECKey;
import co.ambisafe.utils.ECKey.ECDSASignature;
import co.ambisafe.utils.Sha256Hash;
import co.ambisafe.utils.PBKDF2SHA512;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.spongycastle.crypto.InvalidCipherTextException;

public class AccountService {

	/**
	 * Static method that creates an account and save it. This supposed to
	 * happen after user have filled registration form and clicked submit.
	 *
	 * @param {string}
	 *            currency as string
	 * @param {string}
	 *            password as string
	 * @param {string}
	 *            salt as string
	 * @return {Ambisafe.Account} return the generated account object
	 * @throws IOException
	 * @throws IllegalBlockSizeException
	 * @throws ShortBufferException
	 * @throws BadPaddingException
	 * @throws InvalidCipherTextException
	 */
	public static Account generateAccount(String currency, String password, String salt) throws Exception {
		Account account;
		String key;

		CryptoService cryptoService = new CryptoService();

		if (currency == null || password == null) {
			throw new Exception("Currency and password are required");
		}

		if (salt == null) {
			salt = cryptoService.getSalt();
		}

		key = deriveKey(password, salt, 1000);

		account = new Account();
		account.set("key", key);
		account.set("salt", salt);

		if (currency != null) {
			account.set("currency", currency);
		}

		ECKey ecKey = new ECKey();
		account.set("privateKey", ecKey.getPrivateKeyAsHex());
		account.set("publicKey", ecKey.getPublicKeyAsHex());
		account.set("iv", Base16Encoder.encode(cryptoService.getRandomIV()));

		account.set("data", Base16Encoder.encode(cryptoService.encryptData(
				Base16Encoder.decode(ecKey.getPrivateKeyAsHex()),
				Base16Encoder.decode((String) account.get("iv")),
				salt, 
				key
		)));

		return account;
	};

	/**
	 * Static method that signs a transaction.
	 *
	 * @param {object}
	 *            unsigned transaction: {hex:"...", fee:"...", sighashes:["...",
	 *            "..."]}.
	 * @param {string}
	 *            private key.
	 * @return {object} signed transaction.
	 * @throws Exception 
	 */
	@SuppressWarnings("unchecked")
	public static JSONObject signTransaction(JSONObject tx, String privateKey) throws Exception {
		JSONArray sighashes = (JSONArray) tx.get("sighashes");

		if (sighashes == null) {
			throw new Exception("he 'sighashes' attribute is required.");
		}

		JSONArray userSignature = new JSONArray();
		BigInteger privateKeyBigInteger = new BigInteger(Base16Encoder.decode(privateKey));
		ECKey keyPair = ECKey.fromPrivate(privateKeyBigInteger);
		ECDSASignature sign;

		for (int i = 0; i < sighashes.size(); i++) {
			sign = keyPair.sign(Sha256Hash.wrap(sighashes.get(i).toString()));
			userSignature.add(Base16Encoder.encode(sign.encodeToDER()));
		}

		tx.put("user_signature", userSignature);

		return tx;
	};
	
	/**
	 * Static method that derives a key from a password
	 *
	 * @param {string}
	 *            password
	 * @param {string}
	 *            salt
	 * @param {number}
	 *            depth
	 * @return {string} key
	 */
	public static String deriveKey(String password, String salt, int depth) {

		if (depth == 0) {
			depth = 1000;
		}

		byte[] key = PBKDF2SHA512.derive(password, salt, depth, 512 / 32);

		return Base16Encoder.encode(key);
	};

	/**
	 * Static method that gets the SHA1 hash of a string
	 *
	 * @param {string}
	 *            input
	 * @return {string} SHA1 hash
	 * @throws Exception
	 */
	public static String SHA1(String input) throws Exception {

		MessageDigest crypt = MessageDigest.getInstance("SHA-1");
		crypt.reset();
		crypt.update(input.getBytes("UTF-8"));

		return byteToHex(crypt.digest());
	};

	/**
	 * Static method that converts byte to hex
	 *
	 * @param {byte[]}
	 *            hash
	 * @return {string} hex
	 * @throws Exception
	 */
	private static String byteToHex(final byte[] hash) {
		Formatter formatter = new Formatter();
		for (byte b : hash) {
			formatter.format("%02x", b);
		}
		String result = formatter.toString();
		formatter.close();
		return result;
	}

}
