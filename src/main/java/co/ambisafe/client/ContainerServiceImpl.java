package co.ambisafe.client;

import co.ambisafe.utils.Base16Encoder;
import co.ambisafe.utils.ECKey;
import org.spongycastle.crypto.InvalidCipherTextException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.ShortBufferException;
import java.io.IOException;

public class ContainerServiceImpl implements ContainerService {

    @Override
    public Container createContainer(int role, String secret) {
        ECKey ecKey = new ECKey();
        return createContainer(ecKey, role, secret);
    }

    @Override
    public Container createContainer(ECKey ecKey, int role, String secret) {
        CryptoService cryptoService = new CryptoService();
        byte[] containerData;
        byte[] iv = cryptoService.getRandomIV();
        String salt = cryptoService.getSalt();
        try {
            containerData = cryptoService.encryptData(ecKey.getPrivKeyBytes(), iv, salt, secret);
        } catch (InvalidCipherTextException | ShortBufferException | BadPaddingException | IllegalBlockSizeException | IOException e) {
            throw new RuntimeException("Error occurred on attempt to encrypt KeyServer container", e);
        }

        return new Container(role, ecKey.getPublicKeyAsHex(), Base16Encoder.encode(containerData), salt, Base16Encoder.encode(iv));
    }
}
