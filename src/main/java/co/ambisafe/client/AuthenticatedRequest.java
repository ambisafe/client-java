package co.ambisafe.client;

import org.apache.http.HttpHeaders;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.*;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.Clock;

public class AuthenticatedRequest {
    public enum METHOD {GET, POST, PUT, DELETE};

    private final static Logger LOG = LoggerFactory.getLogger(AuthenticatedRequest.class);

    private static CloseableHttpClient httpClient = HttpClients.custom().build();

    public static HttpResponse makeAndExecute(final String url,
                                              final METHOD method,
                                              final String content,
                                              final String key,
                                              final String secret) throws IOException {

        if(url == null) {
            throw new IllegalArgumentException("url is null");
        }

        if(method == null) {
            throw new IllegalArgumentException("method is null");
        }

        if (content == null) {
            throw new IllegalArgumentException("content is null");
        }

        if (key == null) {
            throw new IllegalArgumentException("key is null");
        }

        if (secret == null) {
            throw new IllegalArgumentException("secret is null");
        }

        HttpUriRequest request;

        if (METHOD.GET.equals(method)) {
            request = new HttpGet(url);
        } else if (METHOD.POST.equals(method)) {
            HttpPost postRequest = new HttpPost(url);
            postRequest.setEntity(new StringEntity(content, ContentType.APPLICATION_JSON));
            request = postRequest;
        } else if (METHOD.PUT.equals(method)) {
            HttpPut putRequest = new HttpPut(url);
            putRequest.setEntity(new StringEntity(content, ContentType.APPLICATION_JSON));
            request = putRequest;
        } else if (METHOD.DELETE.equals(method)) {
            request = new HttpDelete(url);
        } else {
            throw new IllegalArgumentException();
        }
        setHttpHeaders(request, url, content, method, key, secret);
        LOG.info("Executing {} request", method.toString());
        return httpClient.execute(request);
    }

    private static void setHttpHeaders(HttpRequest request,
                                       String url,
                                       String content,
                                       METHOD method,
                                       String key,
                                       String secret) {

        LOG.info("Calculating HMAC signature");
        // get nonce
        long currentTime = Clock.systemUTC().millis();
        String timestamp = String.valueOf(currentTime);
        String cannonicalString = HMACUtils.calculateContentToSign(url, method.toString(), content, timestamp);
        String signature = HMACUtils.calculateHMAC(secret, cannonicalString);
        LOG.info("Adding headers");
        request.addHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        request.addHeader("API-Key", key);
        request.addHeader("timestamp", timestamp);
        request.addHeader("Signature", signature);
    }
}
