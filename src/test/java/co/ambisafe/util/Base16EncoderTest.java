package co.ambisafe.util;

import co.ambisafe.utils.Base16Encoder;
import org.junit.Assert;
import org.junit.Test;

public class Base16EncoderTest {
    @Test
    public void test() {
        String val = "9f1c4362cf11c5264c81330210a5c9715daf99cf54afe0d58eb86087eaa512a7";

        byte [] decoded = Base16Encoder.decode(val);

        String encoded = Base16Encoder.encode(decoded);

        Assert.assertEquals(val, encoded);
    }
}
