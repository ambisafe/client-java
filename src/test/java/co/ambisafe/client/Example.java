package co.ambisafe.client;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Example {

    private static ObjectMapper mapper = new ObjectMapper();

    @Test
    public void createContainer() throws Exception {
        ContainerServiceImpl containerService = new ContainerServiceImpl();
        Container user = containerService.createContainer(0, "test");
        Map<String, Container> containers = new HashMap<>();
        containers.put("user", user);
        containers.put("user2", user);

        String id = "test_simple";
        String currency = "ETH";
        String schema = "Simple";

//        createAccount(id, currency, schema, containers);
//        buildTransaction(id, currency);

//        signTransaction();
    }

    @Test
    public void Simple_createAccount() throws IOException {
        String id = "test_simple";
        String currency = "ETH";
        String schema = "Simple";

        Map<String, String> requestMap = new HashMap<>();
        requestMap.put("id", id);
        requestMap.put("currency", currency);
        requestMap.put("security_schema", schema);

        String request = mapper.writeValueAsString(requestMap);

        HttpResponse response = AuthenticatedRequest.makeAndExecute(
                "http://localhost:8080/accounts", AuthenticatedRequest.METHOD.POST, request, "demo", "demo");
        print(response);
    }

    @Test
    public void Simple_buildTransction() throws IOException {
        String id = "test_simple";
        String currency = "ETH";

        String destination = "a085e2f5b4d6e8e611853ad585a1b6c444116ce2";
        String amount = "1";

        Map<String, String> requestMap = new HashMap<>();
        requestMap.put("destination", destination);
        requestMap.put("amount", amount);

        String request = mapper.writeValueAsString(requestMap);

        HttpResponse response = AuthenticatedRequest.makeAndExecute(
                "http://localhost:8080/transactions/build/" + id + "/" + currency, AuthenticatedRequest.METHOD.POST, request, "demo", "demo");
        print(response);
    }

    @Test
    public void Simple_submitTransaction() throws IOException {
        String id = "test_simple";
        String currency = "ETH";

        Map<String, Object> requestMap = new HashMap<>();
        requestMap.put("hex", "e50a850c1b7108008303d09094a085e2f5b4d6e8e611853ad585a1b6c444116ce20180808080");
        requestMap.put("sighashes", new String[] {"f7cc0e6a67151faac9f71c4f296938136f1e76dc2190903a0cd36c5ac1322e9c"});

        String request = mapper.writeValueAsString(requestMap);

        HttpResponse response = AuthenticatedRequest.makeAndExecute(
                "http://localhost:8080/transactions/submit/" + id + "/" + currency, AuthenticatedRequest.METHOD.POST, request, "demo", "demo");
        print(response);
    }

    // =======================================================
    @Test
    public void EToken_createAccount() throws IOException {
        String id = "MP31_test_1";
        String currency = "MP31";
        String schema = "Simple";

        Map<String, String> requestMap = new HashMap<>();
        requestMap.put("id", id);
        requestMap.put("currency", currency);
        requestMap.put("security_schema", schema);

        String request = mapper.writeValueAsString(requestMap);

        HttpResponse response = AuthenticatedRequest.makeAndExecute(
                "http://localhost:8080/accounts", AuthenticatedRequest.METHOD.POST, request, "demo", "demo");
        print(response);
    }

    @Test
    public void EToken_getBalance() throws IOException {
        String id = "MP31_test_1";
        String currency = "MP31";

        HttpResponse response = AuthenticatedRequest.makeAndExecute(
                "http://localhost:8080/balances/" + currency + "/" + id, AuthenticatedRequest.METHOD.GET, "", "demo", "demo");
        print(response);

//        for (int i = 0; i < 40; i++) {
//            EToken_buildAndSubmit();
//        }
    }

    @Test
    public void EToken_buildAndSubmit() throws IOException {
        String id = "etoken_test_1";
        String currency = "ETH";

//        String destination = "0xc650539f3f78044bbd6eeeb4332eff903931d5d4";
        String destination = "e785cee67ef14577ac000d1322fb5c073e343f26";
        String amount = "0.0000001";

        Map<String, Object> requestMap = new HashMap<>();
        requestMap.put("destination", destination);
        requestMap.put("amount", amount);
        requestMap.put("reference", "testReference");

        String request = mapper.writeValueAsString(requestMap);

        HttpResponse response = AuthenticatedRequest.makeAndExecute(
                "http://localhost:8080/transactions/build/" + id + "/" + currency, AuthenticatedRequest.METHOD.POST, request, "demo", "demo");

        JsonNode jsonNode = mapper.readValue(EntityUtils.toString(response.getEntity()), JsonNode.class);

        requestMap = new HashMap<>();
        requestMap.put("hex", jsonNode.path("hex").asText());
        requestMap.put("sighashes", new String[] {jsonNode.path("sighashes").get(0).asText()});
        request = mapper.writeValueAsString(requestMap);

        response = AuthenticatedRequest.makeAndExecute(
                "http://localhost:8080/transactions/submit/" + id + "/" + currency, AuthenticatedRequest.METHOD.POST, request, "demo", "demo");
        print(response);
    }

    @Test
    public void Etoken_buildTransction() throws IOException {
        String id = "MP31_test_1";
        String currency = "MP31";

        String destination = "17xmLaj4LY4CEZKwbNpTFb9vbeXGfqQ4dV";
        String amount = "1";

        Map<String, Object> requestMap = new HashMap<>();
        requestMap.put("destination", destination);
        requestMap.put("amount", amount);
        requestMap.put("reference", "testReference");

        String request = mapper.writeValueAsString(requestMap);

        HttpResponse response = AuthenticatedRequest.makeAndExecute(
                "http://localhost:8080/transactions/build/" + id + "/" + currency, AuthenticatedRequest.METHOD.POST, request, "demo", "demo");
        print(response);
    }

    @Test
    public void EToken_submitTransaction() throws IOException {
        String id = "etoken_test_1";
        String currency = "KUN";

        Map<String, Object> requestMap = new HashMap<>();
        requestMap.put("hex", "f8ec80850c1b7108008303d090940e51acca5bb4d4857cb3d127563fd68497f3438082044ab8c46e293817000000000000000000000000e785cee67ef14577ac000d1322fb5c073e343f26000000000000000000000000000000000000000000000000000000000000044a4b554e00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000080000000000000000000000000000000000000000000000000000000000000000d746573745265666572656e636500000000000000000000000000000000000000808080");
        requestMap.put("sighashes", new String[] {"29fdbcb5f9db251aa593e794dec323e286c2aa51eef4685fe6be3e0ebeb62048"});

        String request = mapper.writeValueAsString(requestMap);

        HttpResponse response = AuthenticatedRequest.makeAndExecute(
                "http://localhost:8080/transactions/submit/" + id + "/" + currency, AuthenticatedRequest.METHOD.POST, request, "demo", "demo");
        print(response);
    }

//    @Test
//    public void createAccountNPlus1() throws IOException {
//        ContainerServiceImpl containerService = new ContainerServiceImpl();
//        Container user = containerService.createContainer(0, "test");
//        Map<String, Container> containers = new HashMap<>();
//        containers.put("user", user);
//
//        String id = "nplus1_id";
//        String currency = "ETOKEN";
//        String schema = "NPlus1";
//
//        createAccount(id, currency, schema, containers);
//    }
//
//    @Test
//    public void signTransactionNPlus1() throws IOException {
//        signTransaction("", "nplus1_id", "ETOKEN");
//    }
//
//    private void createAccount(String id, String currency, String schema, Map<String, Container> containers) throws IOException {
//        String newAccountJson =
//                "{\"id\":\"" + id +
//                "\",\"currency\":\"" + currency +
//                "\",\"security_schema\":\"" + schema + "\"" +
//                ",\"containers\":" + mapper.writeValueAsString(containers) +
//                "}\n";
//        System.out.println("Create account");
//        HttpResponse response = AuthenticatedRequest.makeAndExecute(
//                "http://localhost:8080/accounts", AuthenticatedRequest.METHOD.POST, newAccountJson, "demo", "demo");
//        print(response);
//    }
//
//    private void updateAccount(String id, String currency, String schema, Map<String, Container> containers) throws IOException {
//        String newAccountJson = "{\"id\":\"" + id + "\",\"currency\":\"" + currency + "\",\"security_schema\":\"" + schema + "\",\"containers\":" + mapper.writeValueAsString(containers) + "}\n";
//        System.out.println("Update account");
//        HttpResponse response = AuthenticatedRequest.makeAndExecute(
//                "http://localhost:8080/accounts", AuthenticatedRequest.METHOD.PUT, newAccountJson, "demo", "demo");
//        print(response);
//    }
//
//    private void buildTransaction(String id, String currency) throws IOException {
//        String buildTxJson =
//                "{\n" +
//                "  \"destination\": \"3Cpkfmff1cvXEFaHfMiekGiuPoQJXhMB8H\"," +
//                "  \"amount\": \"1.01\"\n" +
//                "}";
//
//        HttpResponse response = AuthenticatedRequest.makeAndExecute(
//                "http://localhost:8080/transactions/build/" + id + "/" + currency, AuthenticatedRequest.METHOD.POST, buildTxJson, "test", "test");
//        print(response);
//    }
//
//    private void signTransaction(String hash, String id, String currencySymbol) throws IOException {
//        // for test
////        hash = "73020cb8c25f434e00473dcd71be5bcfc0adaf107e0b0f36499d5abf8bd2da18";
////        String signJson = "{\"hex\":\"" + txHex + "\", \"hash\":\"" + hash + "\"}";
//        String signJson = "{\"data\":\"message_to_sign\"}";
//
//        HttpResponse response = AuthenticatedRequest.makeAndExecute("http://localhost:8080/transactions/sign/" + id + "/" + currencySymbol, AuthenticatedRequest.METHOD.POST, signJson, "demo", "demo");
//        print(response);
//    }

    public static void print(HttpResponse response) throws IOException {
        print(EntityUtils.toString(response.getEntity()));
    }

    public static void print(String json) throws IOException {
        System.out.println("JSON: " + json);
        Object obj = mapper.readValue(json, Object.class);
        System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj));
    }
}