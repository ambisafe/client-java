package co.ambisafe.client;

import org.apache.http.HttpResponse;
import org.junit.Test;

import static co.ambisafe.client.AuthenticatedRequest.METHOD.POST;
import static co.ambisafe.client.AuthenticatedRequest.makeAndExecute;
import static co.ambisafe.test.PrintUtils.print;

public class ManageTest {

    private static String baseUrl = "http://35.184.172.0:8080";
    private static String currencySymbol = "BTC";
    private static String blockNumber = "513155";
    private static String key = "ambisafe2018";
    private static String secret = "ambisafe2018";

    @Test
    public void reparseBlock() throws Exception {
        String endpoint = baseUrl + "/manage/reparse_block/" + currencySymbol + "/" + blockNumber;
        HttpResponse response = makeAndExecute(endpoint, POST, "", key, secret);
        print(response);
    }
}
