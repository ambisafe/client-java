package co.ambisafe.client;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

import java.io.IOException;
import java.util.*;

@SuppressWarnings("ALL")
public class Wallet3Test {
    private static ObjectMapper mapper = new ObjectMapper();
    private static String schema = "Simple";
    private static String id = "etoken2_test_amb_1";
    private static String currency = "AMB";

    //    private static String url = "http://keyserver.ambisafe.co:8080";
//    private static String key = "spaceageinvestmentgroup";
//    private static String secret = "QdpKwhG7MmbbEBjdE37p8m2McgaPfDHJ";

    private static String url = "http://localhost:8080";
    private static String key = "demo";
    private static String secret = "demo";

    @Test
    public void createAccount() throws IOException {
        ContainerServiceImpl containerService = new ContainerServiceImpl();
        Map<String, Container> containers = new HashMap<>();

//        Container user = containerService.createContainer(0, "pass_user");
//        Container operator = containerService.createContainer(1, "pass_operator");
//
//        containers.put("user", user);
//        containers.put("operator", operator);

        Map<String, Object> requestMap = new HashMap<>();
        requestMap.put("id", id);
        requestMap.put("currency", currency);
        requestMap.put("security_schema", schema);
//        requestMap.put("containers", containers);

        // 1
        String request = mapper.writeValueAsString(requestMap);

        HttpResponse response = AuthenticatedRequest.makeAndExecute(
                url + "/accounts", AuthenticatedRequest.METHOD.POST, request, key, secret);
        print(response);
    }

    @Test
    public void getBalance() throws IOException {
        HttpResponse response = AuthenticatedRequest.makeAndExecute(
                url + "/balances/" + currency + "/" + id, AuthenticatedRequest.METHOD.GET, "", key, secret);
        print(response);
    }

    @Test
    public void getBalanceByAddress() throws IOException {
        String currency = "ETH";
        String id = "ad3e6c8da7f4ac36ea8e7a0349223bcbc0f9d7f3";
        HttpResponse response = AuthenticatedRequest.makeAndExecute(
                "http://localhost:8080/balances/" + currency + "/address/" + id, AuthenticatedRequest.METHOD.GET, "", "demo", "demo");
        print(response);
    }

    @Test
    public void updateAccount() throws IOException {
        ContainerServiceImpl containerService = new ContainerServiceImpl();

        Container user = containerService.createContainer(0, "pass_user");
        Container operator = containerService.createContainer(1, "pass_operator");

        Map<String, Container> containers = new HashMap<>();
        containers.put("user", user);
        containers.put("operator", operator);

        Map<String, Object> requestMap = new HashMap<>();
        requestMap.put("id", id);
        requestMap.put("currency", currency);
        requestMap.put("security_schema", schema);
        requestMap.put("containers", containers);

        String request = mapper.writeValueAsString(requestMap);

        HttpResponse response = AuthenticatedRequest.makeAndExecute(
                "http://localhost:8080/accounts", AuthenticatedRequest.METHOD.PUT, request, "demo", "demo");
        print(response);
    }

    @Test
    public void buildTransaction() throws IOException {
        String destination = "22a812be80cad76328c07061ad4b43c62d37f9dc";
        // minNonDust
        String amount = "0.01";

        Map<String, Object> requestMap = new HashMap<>();
        requestMap.put("destination", destination);
        requestMap.put("amount", amount);
//        requestMap.put("empty_address", true);

        String request = mapper.writeValueAsString(requestMap);

        HttpResponse response = AuthenticatedRequest.makeAndExecute(
                url + "/transactions/build/" + id + "/" + currency, AuthenticatedRequest.METHOD.POST, request, "demo", "demo");
        print(response);
    }

    @Test
    public void buildAndSubmitTransaction() throws IOException {
        String destination = "22a812be80cad76328c07061ad4b43c62d37f9dc";
        // minNonDust
        String amount = "0.01";

        Map<String, Object> requestMap = new HashMap<>();
        requestMap.put("destination", destination);
        requestMap.put("amount", amount);
        requestMap.put("reference", "Test reference");

        String request = mapper.writeValueAsString(requestMap);

        HttpResponse response = AuthenticatedRequest.makeAndExecute(
                url + "/transactions/build/" + id + "/" + currency, AuthenticatedRequest.METHOD.POST, request, "demo", "demo");

        JsonNode jsonNode = mapper.readValue(EntityUtils.toString(response.getEntity()), JsonNode.class);

        requestMap = new HashMap<>();
        requestMap.put("hex", jsonNode.path("hex").asText());
        Iterator<JsonNode> it = jsonNode.withArray("sighashes").elements();
        List<String> sighashes = new ArrayList<>();
        while (it.hasNext()) {
            sighashes.add(it.next().asText());
        }
        requestMap.put("sighashes", sighashes.toArray());

        request = mapper.writeValueAsString(requestMap);

        response = AuthenticatedRequest.makeAndExecute(
                url + "/transactions/submit/" + id + "/" + currency, AuthenticatedRequest.METHOD.POST, request, "demo", "demo");
        print(response);
    }

    @Test
    public void testRecovery() throws IOException {
        String recoveryTo = "1MmBC6gC3fJKVMu5jnoZjZudMkifZW5PRQ";

        HttpResponse response = AuthenticatedRequest.makeAndExecute(
                "http://localhost:8080/transactions/build_recovery/" + id + "/" + currency + "/" + recoveryTo, AuthenticatedRequest.METHOD.POST, "", "demo", "demo");
        print(response);
    }

    @Test
    public void getContainers() throws IOException {
        HttpResponse response = AuthenticatedRequest.makeAndExecute(
                url + "/accounts/" + id + "/" + currency, AuthenticatedRequest.METHOD.GET, "", "demo", "demo");
        print(response);
    }

    public static void print(HttpResponse response) throws IOException {
        print(EntityUtils.toString(response.getEntity()));
    }

    public static void print(String json) throws IOException {
        System.out.println("JSON: " + json);
        Object obj = mapper.readValue(json, Object.class);
        System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj));
    }
}
