package co.ambisafe.client;

import static co.ambisafe.client.AuthenticatedRequest.METHOD.GET;
import static co.ambisafe.client.AuthenticatedRequest.makeAndExecute;
import static co.ambisafe.test.PrintUtils.print;
import static org.junit.Assert.*;

import co.ambisafe.test.PrintUtils;
import co.ambisafe.utils.Base16Encoder;
import org.apache.http.HttpResponse;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.Test;

public class AccountServiceTest {
    private static String baseUrl = "http://35.184.172.0:8082";
    private static String address = "3BkFCPq4rypPMV4j9sZU7zCs9WLAHkC3Ys";
    private static String key = "ambisafe2018";
    private static String secret = "ambisafe2018";

    @Test
    public void getAccount() throws Exception {
        String endpoint = baseUrl + "/accounts/" + address;
        HttpResponse response = makeAndExecute(endpoint, GET, "", key, secret);
        print(response);
    }

    @Test
    public void testAccountCreationAndChangePassword() throws Exception {
    	CryptoService cryptoService = new CryptoService();
    	String salt = cryptoService.getSalt();
    	Account account = AccountService.generateAccount("BTC", "password", salt);

    	String data1 = Base16Encoder.encode(cryptoService.decryptData(Base16Encoder.decode((String) account.get("data")),
				Base16Encoder.decode((String) account.get("iv")), (String) account.get("salt"), (String) account.get("key")));
    	account.setNewPassword("password1");

    	String data2 = Base16Encoder.encode(cryptoService.decryptData(Base16Encoder.decode((String) account.get("data")),
				Base16Encoder.decode((String) account.get("iv")), (String) account.get("salt"), (String) account.get("key")));
    	
    	assertEquals(data1, data2);
    }
    
    @Test
    public void testDeriveKey() throws Exception {
    	String key = AccountService.deriveKey("password", AccountService.SHA1("password"), 2000);
    	assertEquals("996267ed9e668b27b6977aa9a361c1ea", key);
    }

    @Test
    public void testSignTransaction() throws Exception {
    	CryptoService cryptoService = new CryptoService();
    	String salt = cryptoService.getSalt();
    	Account account = AccountService.generateAccount("BTC", "password", salt);
 
    	String txString = "{\"hex\": \"2340\",\"fee\": \"0.0001 BTC\",\"sighashes\": [\"73020cb8c25f434e00473dcd71be5bc"
    			+ "fc0adaf107e0b0f36499d5abf8bd2da18\"]}";

    	JSONParser parser = new JSONParser();
    	JSONObject tx = (JSONObject) parser.parse(txString);
    	account.set("privateKey", "9f1c4362cf11c5264c81330210a5c9715daf99cf54afe0d58eb86087eaa512a7");
    	account.set("publicKey", "02d53a1d12153dcac231e3c70725134c0fdf9f135e528bb0fd5177dd1def32dc5a");
    	JSONObject signedTx = account.signTransaction(tx);
    	
    	String signedTxResults = "{\"user_signature\":[\"304402203f9d4ba19b8675653a6a6fe76a6b3e55cd545636bd1"
    			+ "c2d384377d48d5042164302207b0f2f4557b343e4fd2847ce542fc44c23ccdebc5fac1f54bf0a94c180187c29\"],\"sigha"
    			+ "shes\":[\"73020cb8c25f434e00473dcd71be5bcfc0adaf107e0b0f36499d5abf8bd2da18\"],\"fee\":\"0.0001 BTC\""
    			+ ",\"hex\":\"2340\"}";

    	assertEquals(signedTxResults,signedTx.toJSONString()); 
    }
}