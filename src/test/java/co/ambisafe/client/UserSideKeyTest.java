package co.ambisafe.client;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.io.IOException;
import java.util.*;

import static co.ambisafe.test.PrintUtils.print;

public class UserSideKeyTest {

    private static ObjectMapper mapper = new ObjectMapper();
//    private static String schema = "Simple";
//    private static String id = "simple_test_1";
//    private static String currency = "BTC";

//    private static String url = "http://localhost:8080";
//    private static String key = "demo";
//    private static String secret = "demo";

    private static String url = "http://35.184.172.0:8082";
    private static String key = "ambisafe2018";
    private static String secret = "ambisafe2018";

    private static String schema = "UserSideKey";
    private static String id = "usersidekey_test_1";
    private static String currency = "BTC";

    @Test
    public void createAccount() throws IOException {
        ContainerServiceImpl containerService = new ContainerServiceImpl();
        Map<String, Container> containers = new HashMap<>();

        Container user = containerService.createContainer(0, "test1");
        user.setPublicKey("04de74461a43afc7865ac2683cdc4546be3535ec396e92041484bb6d74ff8fe8c9046eca4bb9c9d971bd13ca6e2e63c285b22635764a2392475e7c0f6ba11539a9");

        Container operator = containerService.createContainer(1, "test2");
        operator.setPublicKey("046df80ba276dbdc1fbb10c64c8682d734405ea49a8cd6a4b04160ac970b4fc0886b785e79bd2e42374330012c071cb989d43bc92a9ff680907169c72a5ab811e1");

//        .body("containers.USER.role", Matchers.is(roles.getName("USER")))
//        .body("containers.USER.public_key", Matchers.is("04de74461a43afc7865ac2683cdc4546be3535ec396e920414" +
//                "84bb6d74ff8fe8c9046eca4bb9c9d971bd13ca6e2e63c285b22635764a2392475e7c0f6ba11539a9"))
//        .body("containers.USER.salt", Matchers.is("40334160-52ef-4e79-84e3-32cbeeb1fda9"))
//        .body("containers.USER.iv", Matchers.is("920c719bd6dd905e9fec3f6d03ada5b0"))
//        .body("containers.USER.data", Matchers.is("c2f36ff7eb5cd94ed16555dce594aee630fbf8c3246377589d4b175" +
//                "3eb7a2549524f01816afd0badf58d153dd44b2b082a9bd642d31c595d15bfb1991717b793ffa1959596fb64a5" +
//                "942d575aeaf1f414"))

        containers.put("user", user);
        containers.put("operator", operator);

        Map<String, Object> requestMap = new HashMap<>();
        requestMap.put("id", id);
        requestMap.put("currency", currency);
        requestMap.put("security_schema", schema);
        requestMap.put("containers", containers);

        String request = mapper.writeValueAsString(requestMap);

        HttpResponse response = AuthenticatedRequest.makeAndExecute(
                url + "/accounts", AuthenticatedRequest.METHOD.POST, request, key, secret);
        print(response);
    }

    @Test
    public void getBalance() throws IOException {
        HttpResponse response = AuthenticatedRequest.makeAndExecute(
                url + "/balances/" + currency + "/" + id, AuthenticatedRequest.METHOD.GET, "", key, secret);
        print(response);
    }

    @Test
    public void getBalanceByAddress() throws IOException {
        String currency = "BTC";
        String id = "1PPkf1B6AcsQzhk2nV4yQXK8YjkNLVLgkU";
        HttpResponse response = AuthenticatedRequest.makeAndExecute(
                url + "/balances/" + currency + "/address/" + id, AuthenticatedRequest.METHOD.GET, "", key, secret);
        print(response);
    }

    @Test
    public void getContainers() throws IOException {
        HttpResponse response = AuthenticatedRequest.makeAndExecute(
                url + "/accounts/" + id +"/" + currency, AuthenticatedRequest.METHOD.GET, "", key, secret);
        print(response);
    }

    @Test
    public void updateAccount() throws IOException {
        ContainerServiceImpl containerService = new ContainerServiceImpl();
        Container user = containerService.createContainer(0, "test");
        Map<String, Container> containers = new HashMap<>();
        containers.put("user", user);

        Map<String, Object> requestMap = new HashMap<>();
        requestMap.put("id", id);
        requestMap.put("currency", currency);
        requestMap.put("security_schema", schema);
        requestMap.put("containers", containers);

        String request = mapper.writeValueAsString(requestMap);

        HttpResponse response = AuthenticatedRequest.makeAndExecute(
                "http://localhost:8080/accounts", AuthenticatedRequest.METHOD.PUT, request, "demo", "demo");
        print(response);
    }

    @Test
    public void getHistoricalAddresses() throws IOException {
        HttpResponse response = AuthenticatedRequest.makeAndExecute(
                "http://localhost:8080/accounts/" + id + "/" + currency + "/archived_addresses", AuthenticatedRequest.METHOD.GET, "", "demo", "demo");
        print(response);
    }

    @Test
    public void buildTransaction() throws IOException {
        String destination = "14eKAMHoUuozfiWoPuMFXa2g3wTM3MGb1G";
        // minNonDust
        String amount = "0.0000273";

        Map<String, Object> requestMap = new HashMap<>();
        requestMap.put("destination", destination);
        requestMap.put("amount", amount);

        Map<String, Object> options = new HashMap<>();
        requestMap.put("options", options);
        options.put("feePerKb", "0.0001");
        options.put("useUnconfirmed", "false");

        // several destinations
        Map<String, Object> outputs = new HashMap<>();
//        outputs.put("1CopVE7WzsshYuBUgtV75qZWNFRqZ3VAia", amount);

        requestMap.put("outputs", outputs);

        String request = mapper.writeValueAsString(requestMap);

        HttpResponse response = AuthenticatedRequest.makeAndExecute(
                url + "/transactions/build/" + id + "/" + currency, AuthenticatedRequest.METHOD.POST, request, "demo", "demo");
        print(response);
    }

    @Test
    public void buildAndSubmitTransaction() throws IOException {
        String destination = "14eKAMHoUuozfiWoPuMFXa2g3wTM3MGb1G";
        // minNonDust
        String amount = "0.0000273";

        Map<String, Object> requestMap = new HashMap<>();
        requestMap.put("destination", destination);
        requestMap.put("amount", amount);
//        requestMap.put("feePerKb", "0.0008");

        // several destinations
//        Map<String, Object> outputs = new HashMap<>();
//        outputs.put("1CopVE7WzsshYuBUgtV75qZWNFRqZ3VAia", amount);

//        requestMap.put("outputs", outputs);

        String request = mapper.writeValueAsString(requestMap);

        HttpResponse response = AuthenticatedRequest.makeAndExecute(
                "http://localhost:8080/transactions/build/" + id + "/" + currency, AuthenticatedRequest.METHOD.POST, request, "demo", "demo");

        JsonNode jsonNode = mapper.readValue(EntityUtils.toString(response.getEntity()), JsonNode.class);

        requestMap = new HashMap<>();
        requestMap.put("hex", jsonNode.path("hex").asText());
        Iterator<JsonNode> it = jsonNode.withArray("sighashes").elements();
        List<String> sighashes = new ArrayList<>();
        while (it.hasNext()) {
            sighashes.add(it.next().asText());
        }
        requestMap.put("sighashes", sighashes.toArray());
        request = mapper.writeValueAsString(requestMap);

        response = AuthenticatedRequest.makeAndExecute(
                "http://localhost:8080/transactions/submit/" + id + "/" + currency, AuthenticatedRequest.METHOD.POST, request, "demo", "demo");
        print(response);
    }

    @Test
    public void testRecovery() throws IOException {
        String recoveryTo = "1MmBC6gC3fJKVMu5jnoZjZudMkifZW5PRQ";

        HttpResponse response = AuthenticatedRequest.makeAndExecute(
                "http://localhost:8080/transactions/build_recovery/" + id + "/" + currency + "/" + recoveryTo, AuthenticatedRequest.METHOD.POST, "", "demo", "demo");
        print(response);
    }
}
