package co.ambisafe.client;

import co.ambisafe.test.DatabaseUtils;
import co.ambisafe.test.PrintUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import org.apache.http.HttpResponse;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static co.ambisafe.client.AuthenticatedRequest.METHOD.GET;
import static co.ambisafe.client.AuthenticatedRequest.METHOD.POST;
import static co.ambisafe.test.PrintUtils.print;
import static org.junit.Assert.assertEquals;

// https://bitbucket.org/ambisafe/keyserver/issues/8/create-test-for-apikeys
// Description: Need to check that ApiKey B cannot access accounts created by ApiKey A in keyserver
public class ApiKeysSecurityTest {
    private static final String BITCOIN_CURRENCY = "BTC";
    private static final String SECURITY_SCHEMA = "Simple";

    private final static ObjectMapper mapper = new ObjectMapper();

    private final static String BASE_URL = "http://localhost:8080";

    // accounts
    private static Map<String, String> accountExternalIdsMappedByAddress = new HashMap<>();

    // api keys
    private static List<Integer> apiKeyIds = new ArrayList<>();

    // api keys: #1
    private final static String KEY_1 = "api_key_1";
    private final static String SECRET_1 = "api_key_secret_1";

    private final static String KEY_1_ACCOUNT_1_ID = "api_key_1_account_1";
    private final static String KEY_1_ACCOUNT_2_ID = "api_key_1_account_2";
    private final static String KEY_2_ACCOUNT_ID = "api_key_2_account";

    // api keys: #2
    private final static String KEY_2 = "api_key_2";
    private final static String SECRET_2 = "api_key_secret_2";

    @BeforeClass
    public static void beforeClass() throws Exception {
        System.out.println("====================================================");
        System.out.println("After: Create `api_keys`");
        int apiKeyId1 = DatabaseUtils.insertApiKey(KEY_1, SECRET_1);
        int apiKeyId2 = DatabaseUtils.insertApiKey(KEY_2, SECRET_2);
        apiKeyIds.add(apiKeyId1);
        apiKeyIds.add(apiKeyId2);
        System.out.println("====================================================");
        System.out.println("After: Create `accounts`");
        createAccount(KEY_1_ACCOUNT_1_ID, BITCOIN_CURRENCY, SECURITY_SCHEMA, KEY_1, SECRET_1);
        createAccount(KEY_1_ACCOUNT_2_ID, BITCOIN_CURRENCY, SECURITY_SCHEMA, KEY_1, SECRET_1);
        createAccount(KEY_2_ACCOUNT_ID, BITCOIN_CURRENCY, SECURITY_SCHEMA, KEY_2, SECRET_2);
    }

    @AfterClass
    public static void afterClass() throws Exception {
        System.out.println("====================================================");
        System.out.println("After: Delete `accounts`");
        accountExternalIdsMappedByAddress.forEach((accountExternalId, addresses) -> {
            try {
                DatabaseUtils.deleteAccount(accountExternalId);
            } catch (Exception e) {
                System.out.println("Delete account: Failed. " + e.getMessage());
            }
        });
        System.out.println("====================================================");
        System.out.println("After: Delete `api_keys`");
        apiKeyIds.forEach(apiKeyId -> {
            try {
                DatabaseUtils.deleteApiKey(apiKeyId);
            } catch (Exception e) {
                System.out.println("Delete api_key: Failed. " + e.getMessage());
            }
        });
        System.out.println("====================================================");
    }

    // -----------------------------------------------------------------
    // REST API method: getAccountWithArchivedAddresses()
    // -----------------------------------------------------------------

    @Test
    public void getAccountWithArchivedAddresses_AccessOK() throws IOException {
        // Arrange
        String url = BASE_URL + "/accounts/" + KEY_1_ACCOUNT_1_ID + "/" + "BitcoinFamily" + "/archived_addresses";

        // Act
        HttpResponse response = AuthenticatedRequest.makeAndExecute(url, GET, "", KEY_1, SECRET_1);
        String json = PrintUtils.getJson(response);
        print(json, "Get Account Addresses (200, OK)");
        Object document = Configuration.defaultConfiguration().jsonProvider().parse(json);
        String externalId = JsonPath.read(document, "$.account_id");

       // Arrange
        assertEquals(KEY_1_ACCOUNT_1_ID, externalId);
    }

    @Test
    public void getAccountWithArchivedAddresses_AccessForbidden() throws IOException {
        // Arrange
        String url = BASE_URL + "/accounts/" + KEY_2_ACCOUNT_ID + "/" + "BitcoinFamily" + "/archived_addresses";

        // Act
        HttpResponse response = AuthenticatedRequest.makeAndExecute(url, GET, "", KEY_1, SECRET_1);
        String json = PrintUtils.getJson(response);
        print(json, "Get Account Addresses (401, Forbidden)");
        Object document = Configuration.defaultConfiguration().jsonProvider().parse(json);
        int status = JsonPath.read(document, "$.status");

        // Assert
        assertEquals(403, status);
    }

    // -----------------------------------------------------------------
    // REST API method: getAccountWithContainers()
    // -----------------------------------------------------------------

    @Test
    public void getAccountWithContainers_AccessOK() throws IOException {
        // Arrange
        String url = BASE_URL + "/accounts/" + KEY_1_ACCOUNT_1_ID + "/" + BITCOIN_CURRENCY;

        // Act
        HttpResponse response = AuthenticatedRequest.makeAndExecute(url, GET, "", KEY_1, SECRET_1);
        String json = PrintUtils.getJson(response);
        print(json, "Get Account With Container (200, OK)");
        Object document = Configuration.defaultConfiguration().jsonProvider().parse(json);
        String externalId = JsonPath.read(document, "$['account'].externalId");

        // Arrange
        assertEquals(KEY_1_ACCOUNT_1_ID, externalId);
    }

    @Test
    public void getAccountWithContainers_AccessForbidden() throws IOException {
        // Arrange
        String url = BASE_URL + "/accounts/" + KEY_2_ACCOUNT_ID + "/" + BITCOIN_CURRENCY;

        // Act
        HttpResponse response = AuthenticatedRequest.makeAndExecute(url, GET, "", KEY_1, SECRET_1);
        String json = PrintUtils.getJson(response);
        print(json, "Get Account With Container (401, Forbidden)");
        Object document = Configuration.defaultConfiguration().jsonProvider().parse(json);
        int status = JsonPath.read(document, "$.status");

        // Assert
        assertEquals(403, status);
    }

    // -----------------------------------------------------------------
    // REST API method: getAccountByAddress()
    // -----------------------------------------------------------------

    @Test
    public void getAccountByAddress_AccessOK() throws IOException {
        // Arrange
        String address = accountExternalIdsMappedByAddress.get(KEY_1_ACCOUNT_1_ID);
        String url = BASE_URL + "/accounts/" + address;

        // Act
        HttpResponse response = AuthenticatedRequest.makeAndExecute(url, GET, "", KEY_1, SECRET_1);
        String json = PrintUtils.getJson(response);
        print(json, "Get Account By Address (200, OK)");
        Object document = Configuration.defaultConfiguration().jsonProvider().parse(json);
        String externalId = JsonPath.read(document, "$['account'].externalId");

        // Arrange
        assertEquals(KEY_1_ACCOUNT_1_ID, externalId);
    }

    @Test
    public void getAccountByAddress_AccessForbidden() throws IOException {
        // Arrange
        String address = accountExternalIdsMappedByAddress.get(KEY_2_ACCOUNT_ID);
        String url = BASE_URL + "/accounts/" + address;

        // Act
        HttpResponse response = AuthenticatedRequest.makeAndExecute(url, GET, "", KEY_1, SECRET_1);
        String json = PrintUtils.getJson(response);
        print(json, "Get Account By Address (401, Forbidden)");
        Object document = Configuration.defaultConfiguration().jsonProvider().parse(json);
        int status = JsonPath.read(document, "$.status");

        // Assert
        assertEquals(403, status);
    }

    // -----------------------------------------------------------------
    // Private Methods
    // -----------------------------------------------------------------
    private static void createAccount(String id, String currency, String schema, String key, String secret) throws IOException {
        // prepare request
        System.out.println("Create account: " + id + " + " + currency + " + " + schema);
        Map<String, Object> request = new HashMap<>();
        request.put("id", id);
        request.put("currency", currency);
        request.put("security_schema", schema);
        String requestJson = mapper.writeValueAsString(request);

        // execute request
        HttpResponse response = AuthenticatedRequest.makeAndExecute(
                BASE_URL + "/accounts", POST, requestJson, key, secret);
        String json = PrintUtils.getJson(response);
        print(json, "Create Account");
        Object document = Configuration.defaultConfiguration().jsonProvider().parse(json);
        String address = JsonPath.read(document, "$['account'].address");

        // update mapping: account external id <-> address
        accountExternalIdsMappedByAddress.put(id, address);
    }
}
