package co.ambisafe.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class MultiCurrencyTest {

    private static ObjectMapper mapper = new ObjectMapper();
    private static String schema = "UserSideKey";
    private static String id = "omni_test_1";

    @Test
    public void createAccount() throws IOException {
        ContainerServiceImpl containerService = new ContainerServiceImpl();
        Map<String, Container> containers = new HashMap<>();

        Container user = containerService.createContainer(0, "test");

        containers.put("user", user);

        Map<String, Object> requestMap = new HashMap<>();
        requestMap.put("id", id);
        requestMap.put("currency", "MP31");
        requestMap.put("security_schema", schema);
        requestMap.put("containers", containers);

        // 1
        String request = mapper.writeValueAsString(requestMap);

        HttpResponse response = AuthenticatedRequest.makeAndExecute(
                "http://localhost:8080/accounts", AuthenticatedRequest.METHOD.POST, request, "demo", "demo");
        print(response);

        // 2
        requestMap.put("currency", "MP41");
        request = mapper.writeValueAsString(requestMap);

        response = AuthenticatedRequest.makeAndExecute(
                "http://localhost:8080/accounts", AuthenticatedRequest.METHOD.POST, request, "demo", "demo");
        print(response);
    }

    @Test
    public void updateAccount() throws Exception {
        ContainerServiceImpl containerService = new ContainerServiceImpl();
        Map<String, Container> containers = new HashMap<>();

        Container user = containerService.createContainer(0, "test");

        containers.put("user", user);

        Map<String, Object> requestMap = new HashMap<>();
        requestMap.put("id", id);
        requestMap.put("currency", "MP31");
        requestMap.put("security_schema", schema);
        requestMap.put("containers", containers);

        // 1
        String request = mapper.writeValueAsString(requestMap);

        HttpResponse response = AuthenticatedRequest.makeAndExecute(
                "http://localhost:8080/accounts", AuthenticatedRequest.METHOD.PUT, request, "demo", "demo");
        print(response);
    }

    public static void print(HttpResponse response) throws IOException {
        print(EntityUtils.toString(response.getEntity()));
    }

    public static void print(String json) throws IOException {
        System.out.println("JSON: " + json);
        Object obj = mapper.readValue(json, Object.class);
        System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj));
    }
}
