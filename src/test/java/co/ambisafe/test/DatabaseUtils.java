package co.ambisafe.test;

import java.sql.*;
import java.util.stream.IntStream;

public class DatabaseUtils {

    private static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String URL = "jdbc:mysql://localhost:3306/keyserver?tinyInt1isBit=false&useUnicode=true&characterEncoding=UTF-8&connectionCollation=utf8_unicode_ci&characterSetResults=UTF-8&useSSL=false";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "password";

    // -----------------------------------------------------------------
    // Table: api_keys
    // -----------------------------------------------------------------

    public static int insertApiKey(String name, String secret) throws Exception {
        System.out.println("Insert api_key: " + name + " + " + secret);
        Class.forName(JDBC_DRIVER);
        Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        String query = "INSERT INTO api_keys (name, secret, enabled, postback_url, created_at, updated_at, tenant_id) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, name);
        ps.setString(2, secret);
        ps.setBoolean(3, true);
        ps.setString(4, null);
        Date date = new Date(new java.util.Date().getTime());
        ps.setDate(5, date);
        ps.setDate(6, date);
        ps.setString(7, null);
        ps.execute();
        ResultSet rs = ps.getGeneratedKeys();
        int apiKeyId = 0;
        if (rs.next()) {
            apiKeyId = rs.getInt(1);
        }
        connection.close();
        insertApiKeyFullPermissions(apiKeyId);
        System.out.println("Inserted api_key id: " + apiKeyId);
        return apiKeyId;
    }

    public static void deleteApiKey(int apiKeyId) throws Exception {
        System.out.println("Delete api_key by `id`: " + apiKeyId);
        Class.forName(JDBC_DRIVER);
        Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        String query = "DELETE FROM api_keys WHERE id = ?";
        PreparedStatement ps = connection.prepareStatement(query);
        ps.setInt(1, apiKeyId);
        ps.executeUpdate();
        connection.close();
    }

    // -----------------------------------------------------------------
    // Table: accounts
    // -----------------------------------------------------------------
    public static void deleteAccount(String accountExternalId) throws Exception {
        System.out.println("Delete account by `external_id`: " + accountExternalId);
        Class.forName(JDBC_DRIVER);
        Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        String query = "DELETE FROM accounts WHERE external_id = ?";
        PreparedStatement ps = connection.prepareStatement(query);
        ps.setString(1, accountExternalId);
        ps.executeUpdate();
        connection.close();
    }

    // -----------------------------------------------------------------
    // Private Methods
    // -----------------------------------------------------------------
    private static void insertApiKeyFullPermissions(int apiKeyId) throws Exception {
        System.out.println("Insert api_key permissions for apiKeyId: " + apiKeyId);
        Class.forName(JDBC_DRIVER);
        Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        String query = "INSERT INTO api_keys_permissions (api_key_id, permission_id) VALUES (?, ?) ";
        PreparedStatement ps = connection.prepareStatement(query);
        IntStream.rangeClosed(1, 8).forEach(value -> {
            try {
                ps.setInt(1, apiKeyId);
                ps.setInt(2, value);
                ps.addBatch();
            } catch (SQLException e) {
                System.out.println("Insert api_key permissions: Failed. " + e.getMessage());
            }
        });
        ps.executeBatch();
        connection.close();
    }
}
