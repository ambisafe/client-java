package co.ambisafe.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public class PrintUtils {
    private static ObjectMapper mapper = new ObjectMapper();

    public static String getJson(HttpResponse response) throws IOException {
        return EntityUtils.toString(response.getEntity());
    }

    public static void print(HttpResponse response) throws IOException {
        print(getJson(response));
    }

    public static void print(HttpResponse response, String method) throws IOException {
        print(getJson(response), method);
    }

    public static void print(String json) throws IOException {
        System.out.println("Response JSON: ");
        Object obj = mapper.readValue(json, Object.class);
        System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj) + "\n");
    }

    public static void print(String json, String method) throws IOException {
        System.out.println("Response JSON. Method [" + method + "]:");
        Object obj = mapper.readValue(json, Object.class);
        System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj) + "\n");
    }
}
